import json
import pymongo
import requests
import urllib.request
import bson
import csv
from bson.raw_bson import RawBSONDocument


def download_data(url):
    response = urllib.request.urlopen(url)
    content = response.read()
    data = json.loads(content.decode("utf8"))
    return data


def download_data2(url):
    contents = urllib.request.urlopen(url).read()
    contents_str = contents.decode('utf-8')
    data = contents_str.split('\n')
    return data


def create_file_local(namefile, datajason):
    with open(namefile, 'w') as file:
        json.dump(datajason, file)
        print(json.dumps(datajason, indent=4))

def conexion_bbdd(cadena_conexion):
    client = pymongo.MongoClient(cadena_conexion)
    client.test
    return client


def insert_bbdd(bbdd, col, client, data):
    db = client[bbdd]
    coleccion = db[col]
    #coleccion.insert_one(data)
    coleccion.insert_one(bson.son.SON(data))

def get_stations(collection, query):
    data = collection.find(query)
    # for i in data:
    #     print(i)
    return data

if __name__ == '__main__':
    # Parametros
    url_data = 'https://api.bsmsa.eu/ext/api/bsm/gbfs/v2/en/station_status'
    name_file_out = './local_file.json'
    col ='colprueba'
    db = 'bdprueba'

    # LLAMADO A FUNCIONES
    lj_tramos = download_data(url_data)             # Se descargan los datos
    create_file_local(name_file_out, lj_tramos)     # Se crea el archivo  localmente
    myClient = conexion_bbdd("mongodb+srv://carlos:camr0704@clprueba-fjwer.gcp.mongodb.net/test?retryWrites=true&w=majority")  # Conexion a MongoDb
    insert_bbdd(db, col, myClient, lj_tramos)       # Inserta en MongoDB los datos

    stations = get_stations(col, "{ 'data.stations': { $elemMatch: { num_docks_available: 21 } } } ")

